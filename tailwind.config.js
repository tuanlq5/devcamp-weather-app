/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      content:{
        "weatherIcon": 'url("./assets/images/loading.gif")'
      },
      backgroundImage: {
        'sky-background': "url('./assets/images/background.jpg')",
        "02d": "url('./assets/images/02d.svg')",
        "01d": "url('./assets/images/01d.svg')",
        "03d": "url('./assets/images/03d.svg')",
        "04d": "url('./assets/images/04d.svg')",
        "09d": "url('./assets/images/09d.svg')",
        "10d": "url('./assets/images/10d.svg')",
        "11d": "url('./assets/images/11d.svg')",
        "13d": "url('./assets/images/13d.svg')",
        "50d": "url('./assets/images/50d.svg')",
        "02n": "url('./assets/images/02n.svg')",
        "01n": "url('./assets/images/01n.svg')",
        "03n": "url('./assets/images/03n.svg')",
        "04n": "url('./assets/images/04n.svg')",
        "09n": "url('./assets/images/09n.svg')",
        "10n": "url('./assets/images/10n.svg')",
        "11n": "url('./assets/images/11n.svg')",
        "13n": "url('./assets/images/13n.svg')",
        "50n": "url('./assets/images/50n.svg')",
        "cloudyMoon": "url('./assets/images/cloudy_moon.svg)",
        "loading": "url('./assets/images/loading.gif')"
      }
    },
  },
  plugins: [],
}
