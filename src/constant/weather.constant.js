export const USER_TYPE_AND_CLICK = "Khi user nhập tên thành phố";

export const SEND_REQUEST_WEATHER_DATA_PENDING = "Bắt đầu gửi request tới server";

export const SEND_REQUEST_WEATHER_DATA_SUCCESS = "Gửi request tới server thành công";

export const SEND_REQUEST_WEATHER_DATA_FAIL = "Gửi request tới server thất bại";