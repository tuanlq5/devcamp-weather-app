import { useSelector } from "react-redux";
export default function MainContent () {
    const {showInfoWeather , cityWeather} = useSelector((reduxData) => reduxData.weatherReducer);
    return(
        <>  
            {
                showInfoWeather === false? 
                <>
                <div className="invisible block relative bg-01d bg-cover bg-no-repeat w-[300px] h-[300px] transition-all ease-out duration-[2000ms] top-[90px] "></div>
                </>
                :
                <>
                { cityWeather.list[0].weather[0].icon  === "01d" && <div className=" block relative left-[-80px] bg-01d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "01n" && <div className=" block relative left-[-80px] bg-01n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "02d" && <div className=" block relative left-[-80px] bg-02d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "02n" && <div className=" block relative left-[-80px] bg-02n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "03d" && <div className=" block relative left-[-80px] bg-03d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "03n" && <div className=" block relative left-[-80px] bg-03n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "04d" && <div className=" block relative left-[-80px] bg-04d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "04n" && <div className=" block relative left-[-80px] bg-04n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "09d" && <div className=" block relative left-[-80px] bg-09d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "09n" && <div className=" block relative left-[-80px] bg-09n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "10d" && <div className=" block relative left-[-80px] bg-10d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "10n" && <div className=" block relative left-[-80px] bg-10n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "11d" && <div className=" block relative left-[-80px] bg-11d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "11n" && <div className=" block relative left-[-80px] bg-11n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "13d" && <div className=" block relative left-[-80px] bg-13d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "13n" && <div className=" block relative left-[-80px] bg-13n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "50d" && <div className=" block relative left-[-80px] bg-50d bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}
                { cityWeather.list[0].weather[0].icon  === "50n" && <div className=" block relative left-[-80px] bg-50n bg-cover bg-no-repeat w-[300px] h-[300px] transition-opacity ease-out duration-[2000ms] top-[90px]"></div>}

                <div className="w-[350px]  relative top-[90px] flex flex-col items-start justify-center">
                    <p className="text-[30px] text-center" >Today</p>
                    <h1 className="text-[59px] font-bold text-center">{cityWeather.city.name}</h1>
                    <p className="text-[30px] text-center">Temperature: {Math.floor(cityWeather.list[0].main.temp)}°C</p>
                    <p className="text-[30px] text-center">{cityWeather.list[0].weather[0].description}</p>
                </div>
                </>
            }
            
            
        </>
    )
}