import { useSelector } from "react-redux";
//Hàm chuyển format date  => yyyy/mm/dd
function formatDate1(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate() + 1),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
function formatDate2(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate() + 2),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
function formatDate3(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate() + 3),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
function formatDate4(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate() + 4),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

export default function ChildCard (){
    
    const {showInfoWeather, cityWeather, days} = useSelector((reduxData) => reduxData.weatherReducer);
    const date = new Date();
    // console.log(days[date.getDay()]);
    const TOMOROW_DATE = formatDate1(date);
    const NEXT_TOMOROW_DATE = formatDate2(date);
    const NEXT_TOMOROW_DATE_2 = formatDate3(date);
    const NEXT_TOMOROW_DATE_3 = formatDate4(date);
    return(
            <div style={{
                visibility: showInfoWeather === false ? "hidden" : "visible"
            }}>
                <ul className=" w-[850px] relative top-[70px] flex justify-around">
                    <li>
                        <div className="flex flex-col justify-evenly items-center w-[180px] h-[180px] rounded-[30%] bg-[hsla(0,0%,100%,.3)] shadow-[3px_0_8px_silver] pb-[10px]">
                            <p className="text-[20px] mt-[15px] font-bold">Staturday</p>
                            { cityWeather.list[1].weather[0].icon  === "01d" &&  <div className="block bg-01d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "01n" &&  <div className="block bg-01n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "02d" &&  <div className="block bg-02d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "02n" &&  <div className="block bg-02n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "03d" &&  <div className="block bg-03d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "03n" &&  <div className="block bg-03n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "04d" &&  <div className="block bg-04d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "04n" &&  <div className="block bg-04n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "09d" &&  <div className="block bg-09d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "09n" &&  <div className="block bg-09n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "10d" &&  <div className="block bg-10d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "10n" &&  <div className="block bg-10n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "11d" &&  <div className="block bg-11d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "11n" &&  <div className="block bg-11n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "13d" &&  <div className="block bg-13d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "13n" &&  <div className="block bg-13n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "50d" &&  <div className="block bg-50d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[1].weather[0].icon  === "50n" &&  <div className="block bg-50n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            <p className=" text-[25px] leading-none">{Math.floor(cityWeather.list[1].main.temp)}<span>°C</span></p>
                        </div>
                    </li>
                    <li>
                        <div className="flex flex-col justify-evenly items-center w-[180px] h-[180px] rounded-[30%] bg-[hsla(0,0%,100%,.3)] shadow-[3px_0_8px_silver] pb-[10px]">
                            <p className="text-[20px] mt-[15px] font-bold">Sunday</p>
                            { cityWeather.list[2].weather[0].icon  === "01d" &&  <div className="block bg-01d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "01n" &&  <div className="block bg-01n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "02d" &&  <div className="block bg-02d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "02n" &&  <div className="block bg-02n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "03d" &&  <div className="block bg-03d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "03n" &&  <div className="block bg-03n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "04d" &&  <div className="block bg-04d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "04n" &&  <div className="block bg-04n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "09d" &&  <div className="block bg-09d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "09n" &&  <div className="block bg-09n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "10d" &&  <div className="block bg-10d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "10n" &&  <div className="block bg-10n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "11d" &&  <div className="block bg-11d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "11n" &&  <div className="block bg-11n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "13d" &&  <div className="block bg-13d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "13n" &&  <div className="block bg-13n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "50d" &&  <div className="block bg-50d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[2].weather[0].icon  === "50n" &&  <div className="block bg-50n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            <p className=" text-[25px] leading-none">{Math.floor(cityWeather.list[2].main.temp)}<span>°C</span></p>
                        </div>
                    </li>
                    <li>
                        <div className="flex flex-col justify-evenly items-center w-[180px] h-[180px] rounded-[30%] bg-[hsla(0,0%,100%,.3)] shadow-[3px_0_8px_silver] pb-[10px]">
                            <p className="text-[20px] mt-[15px] font-bold">Monday</p>
                            { cityWeather.list[3].weather[0].icon  === "01d" &&  <div className="block bg-01d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "01n" &&  <div className="block bg-01n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "02d" &&  <div className="block bg-02d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "02n" &&  <div className="block bg-02n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "03d" &&  <div className="block bg-03d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "03n" &&  <div className="block bg-03n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "04d" &&  <div className="block bg-04d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "04n" &&  <div className="block bg-04n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "09d" &&  <div className="block bg-09d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "09n" &&  <div className="block bg-09n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "10d" &&  <div className="block bg-10d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "10n" &&  <div className="block bg-10n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "11d" &&  <div className="block bg-11d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "11n" &&  <div className="block bg-11n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "13d" &&  <div className="block bg-13d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "13n" &&  <div className="block bg-13n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "50d" &&  <div className="block bg-50d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[3].weather[0].icon  === "50n" &&  <div className="block bg-50n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            <p className=" text-[25px] leading-none">{Math.floor(cityWeather.list[3].main.temp)}<span>°C</span></p>
                        </div>
                    </li>
                    <li>
                        <div className="flex flex-col justify-evenly items-center w-[180px] h-[180px] rounded-[30%] bg-[hsla(0,0%,100%,.3)] shadow-[3px_0_8px_silver] pb-[10px]">
                            <p className="text-[20px] mt-[15px] font-bold">Tuesday</p>
                            { cityWeather.list[4].weather[0].icon  === "01d" &&  <div className="block bg-01d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "01n" &&  <div className="block bg-01n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "02d" &&  <div className="block bg-02d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "02n" &&  <div className="block bg-02n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "03d" &&  <div className="block bg-03d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "03n" &&  <div className="block bg-03n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "04d" &&  <div className="block bg-04d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "04n" &&  <div className="block bg-04n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "09d" &&  <div className="block bg-09d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "09n" &&  <div className="block bg-09n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "10d" &&  <div className="block bg-10d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "10n" &&  <div className="block bg-10n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "11d" &&  <div className="block bg-11d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "11n" &&  <div className="block bg-11n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "13d" &&  <div className="block bg-13d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "13n" &&  <div className="block bg-13n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "50d" &&  <div className="block bg-50d w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            { cityWeather.list[4].weather[0].icon  === "50n" &&  <div className="block bg-50n w-[100px] h-[100px] bg-no-repeat bg-contain "></div>}
                            <p className=" text-[25px] leading-none">{Math.floor(cityWeather.list[4].main.temp)}<span>°C</span></p>
                        </div>
                    </li>
                </ul>
            </div> 
    )
}