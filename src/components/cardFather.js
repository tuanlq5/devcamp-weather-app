import { useSelector } from "react-redux";
import CityInput from "./input/city.input";
import ChildCard from "./cards/cardChild";
import MainContent from "./mainContent/mainContent";
export default function  CardFather () {
    const {showInfoWeather} = useSelector((reduxData) => reduxData.weatherReducer);
    return(
        <>
            <div className="relative w-[960px] h-[480px] top-[-25px] rounded-[30px] bg-[hsla(0,0%,100%,.2)] shadow-[0_0_10px_silver] flex justify-center items-center flex-col">
                <div className="w-[800px] flex justify-center items-center relative ">
                    {
                        showInfoWeather === false ?
                            <h1 className="  w-[800px] h-[300px] absolute before:inline-block before:w-[250px] before:h-[250px] before:mx-auto before:bg-02d before:bg-contain before:bg-no-repeat text-[4rem] text-[#365a7a] font-bold top-[60px] flex justify-around items-center gap-2 ">
                                Weather Forecast
                            </h1>   : <></>
                    }
                    
                    <MainContent></MainContent>

                </div>
                <CityInput></CityInput>
                <ChildCard></ChildCard>
            </div>
        </>
    )
}; 