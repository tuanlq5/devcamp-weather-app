import { useDispatch, useSelector } from "react-redux"
import { onChangeInputCity, onClickEnterSearchCity } from "../../actions/weather.actions";
export default function CityInput (){
    const dispatch = useDispatch();
    const {showInfoWeather, city, cityWeather} = useSelector((reduxData) => reduxData.weatherReducer);
    const onChangeInput = (event) => {
        dispatch(onChangeInputCity(event.target.value))
    }
    const onEnterClick = (event) => {
        event.preventDefault();
        dispatch(onClickEnterSearchCity(city, cityWeather))
        console.log(showInfoWeather)
    }
    return(
        <form onSubmit={onEnterClick} >
            <input placeholder="Enter a City..." 
            onChange={onChangeInput}
            className="  
            relative
            p-[10px_0_10px_30px] 
            rounded-[20px] 
            w-[600px] 
            border-[2px]
             border-black
            ease-out 
            duration-500 
            focus:outline-none 
            text-[20px]"
            style={
                {   
                    lineHeight: "0px",
                    transition: "ease-out 0.7s" ,
                    top: showInfoWeather === false ? "70px" : "-380px",
                    translateY: showInfoWeather === true? "-100px ": "0"
                }
            }
            >
            </input>
        </form>
    )
}
