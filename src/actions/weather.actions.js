import { 
    USER_TYPE_AND_CLICK,
    SEND_REQUEST_WEATHER_DATA_PENDING,
    SEND_REQUEST_WEATHER_DATA_SUCCESS,
    SEND_REQUEST_WEATHER_DATA_FAIL
} from "../constant/weather.constant";


export const onChangeInputCity = (value) => {
    return{
        type: USER_TYPE_AND_CLICK,
        payload: value
    }
}
export const onClickEnterSearchCity = (value , preWeather) => {
    return async (dispatch) => {
        try{
            var requestOption = {
                method: "GET",
                redirect: "follow"
            }
            await dispatch({
                type: SEND_REQUEST_WEATHER_DATA_PENDING
            })

            const url = await fetch (`https://api.openweathermap.org/data/2.5/forecast?q=${value}&appid=372333dad95290f64df724744a1bec58&units=metric`, requestOption);
            
            const data = await url.json();
            
            if(data.cod === "400" ){
                return dispatch({
                    type: SEND_REQUEST_WEATHER_DATA_SUCCESS,
                    payload: preWeather
                })
            } else if (data.cod === "404" ){
                return dispatch({
                    type: SEND_REQUEST_WEATHER_DATA_SUCCESS,
                    payload: preWeather
                })
            }else{
                return dispatch({
                    type: SEND_REQUEST_WEATHER_DATA_SUCCESS,
                    payload: data
                })
            }
        } catch (error){
            return dispatch({
                type: SEND_REQUEST_WEATHER_DATA_FAIL,
                error: error
            })
        }
    }
}
