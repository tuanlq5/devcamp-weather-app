import CardFather from "./components/cardFather";
import "./App.css"
function App() {
  return (
    <>
    <div className=" w-screen h-screen mx-auto bg-sky-background bg-cover bg-no-repeat flex flex-col bg-bottom justify-center items-center">
      <CardFather></CardFather>
    </div>
    </>
  );
}

export default App;
