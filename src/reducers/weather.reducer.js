import { 
    SEND_REQUEST_WEATHER_DATA_PENDING,
    SEND_REQUEST_WEATHER_DATA_SUCCESS,
    SEND_REQUEST_WEATHER_DATA_FAIL,
    USER_TYPE_AND_CLICK
} from "../constant/weather.constant";
const  JSON_WEATHER = `{
    "cod": "200",
    "message": 0,
    "cnt": 40,
    "list": [
        {
            "dt": 1678471200,
            "main": {
                "temp": 22,
                "feels_like": 22.48,
                "temp_min": 21.04,
                "temp_max": 22,
                "pressure": 1014,
                "sea_level": 1014,
                "grnd_level": 1012,
                "humidity": 85,
                "temp_kf": 0.96
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "clouds": {
                "all": 31
            },
            "wind": {
                "speed": 4.37,
                "deg": 150,
                "gust": 9.58
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-10 18:00:00"
        },
        {
            "dt": 1678482000,
            "main": {
                "temp": 21.4,
                "feels_like": 21.87,
                "temp_min": 20.2,
                "temp_max": 21.4,
                "pressure": 1014,
                "sea_level": 1014,
                "grnd_level": 1012,
                "humidity": 87,
                "temp_kf": 1.2
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "clouds": {
                "all": 45
            },
            "wind": {
                "speed": 3.54,
                "deg": 133,
                "gust": 7.72
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-10 21:00:00"
        },
        {
            "dt": 1678492800,
            "main": {
                "temp": 21.15,
                "feels_like": 21.62,
                "temp_min": 20.73,
                "temp_max": 21.15,
                "pressure": 1015,
                "sea_level": 1015,
                "grnd_level": 1014,
                "humidity": 88,
                "temp_kf": 0.42
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 68
            },
            "wind": {
                "speed": 2.83,
                "deg": 140,
                "gust": 7.28
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-11 00:00:00"
        },
        {
            "dt": 1678503600,
            "main": {
                "temp": 23.88,
                "feels_like": 24.05,
                "temp_min": 23.88,
                "temp_max": 23.88,
                "pressure": 1018,
                "sea_level": 1018,
                "grnd_level": 1016,
                "humidity": 66,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 3.72,
                "deg": 150,
                "gust": 5.27
            },
            "visibility": 10000,
            "pop": 0.25,
            "rain": {
                "3h": 0.14
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-11 03:00:00"
        },
        {
            "dt": 1678514400,
            "main": {
                "temp": 25.45,
                "feels_like": 25.57,
                "temp_min": 25.45,
                "temp_max": 25.45,
                "pressure": 1016,
                "sea_level": 1016,
                "grnd_level": 1014,
                "humidity": 58,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 3.73,
                "deg": 161,
                "gust": 4.75
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-11 06:00:00"
        },
        {
            "dt": 1678525200,
            "main": {
                "temp": 26,
                "feels_like": 26,
                "temp_min": 26,
                "temp_max": 26,
                "pressure": 1014,
                "sea_level": 1014,
                "grnd_level": 1013,
                "humidity": 55,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 3.52,
                "deg": 153,
                "gust": 4.16
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-11 09:00:00"
        },
        {
            "dt": 1678536000,
            "main": {
                "temp": 24.36,
                "feels_like": 24.52,
                "temp_min": 24.36,
                "temp_max": 24.36,
                "pressure": 1016,
                "sea_level": 1016,
                "grnd_level": 1015,
                "humidity": 64,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 3.51,
                "deg": 139,
                "gust": 6.02
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-11 12:00:00"
        },
        {
            "dt": 1678546800,
            "main": {
                "temp": 22.93,
                "feels_like": 23.18,
                "temp_min": 22.93,
                "temp_max": 22.93,
                "pressure": 1018,
                "sea_level": 1018,
                "grnd_level": 1017,
                "humidity": 73,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 3.31,
                "deg": 174,
                "gust": 6.31
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-11 15:00:00"
        },
        {
            "dt": 1678557600,
            "main": {
                "temp": 22.05,
                "feels_like": 22.32,
                "temp_min": 22.05,
                "temp_max": 22.05,
                "pressure": 1017,
                "sea_level": 1017,
                "grnd_level": 1016,
                "humidity": 77,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 94
            },
            "wind": {
                "speed": 2.4,
                "deg": 186,
                "gust": 4.85
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-11 18:00:00"
        },
        {
            "dt": 1678568400,
            "main": {
                "temp": 20.86,
                "feels_like": 21.17,
                "temp_min": 20.86,
                "temp_max": 20.86,
                "pressure": 1017,
                "sea_level": 1017,
                "grnd_level": 1015,
                "humidity": 83,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 98
            },
            "wind": {
                "speed": 2.09,
                "deg": 108,
                "gust": 2.49
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-11 21:00:00"
        },
        {
            "dt": 1678579200,
            "main": {
                "temp": 21.08,
                "feels_like": 21.36,
                "temp_min": 21.08,
                "temp_max": 21.08,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1018,
                "humidity": 81,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 1.33,
                "deg": 143,
                "gust": 1.99
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-12 00:00:00"
        },
        {
            "dt": 1678590000,
            "main": {
                "temp": 24.51,
                "feels_like": 24.66,
                "temp_min": 24.51,
                "temp_max": 24.51,
                "pressure": 1021,
                "sea_level": 1021,
                "grnd_level": 1019,
                "humidity": 63,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.09,
                "deg": 12,
                "gust": 1.05
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-12 03:00:00"
        },
        {
            "dt": 1678600800,
            "main": {
                "temp": 27.56,
                "feels_like": 28.05,
                "temp_min": 27.56,
                "temp_max": 27.56,
                "pressure": 1018,
                "sea_level": 1018,
                "grnd_level": 1017,
                "humidity": 51,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 3.32,
                "deg": 353,
                "gust": 3.4
            },
            "visibility": 10000,
            "pop": 0.03,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-12 06:00:00"
        },
        {
            "dt": 1678611600,
            "main": {
                "temp": 27.3,
                "feels_like": 27.85,
                "temp_min": 27.3,
                "temp_max": 27.3,
                "pressure": 1017,
                "sea_level": 1017,
                "grnd_level": 1015,
                "humidity": 52,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.16,
                "deg": 15,
                "gust": 6.12
            },
            "visibility": 10000,
            "pop": 0.12,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-12 09:00:00"
        },
        {
            "dt": 1678622400,
            "main": {
                "temp": 22.06,
                "feels_like": 21.63,
                "temp_min": 22.06,
                "temp_max": 22.06,
                "pressure": 1021,
                "sea_level": 1021,
                "grnd_level": 1019,
                "humidity": 50,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 8.59,
                "deg": 40,
                "gust": 12.54
            },
            "visibility": 10000,
            "pop": 0.44,
            "rain": {
                "3h": 0.23
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-12 12:00:00"
        },
        {
            "dt": 1678633200,
            "main": {
                "temp": 18.58,
                "feels_like": 17.93,
                "temp_min": 18.58,
                "temp_max": 18.58,
                "pressure": 1024,
                "sea_level": 1024,
                "grnd_level": 1022,
                "humidity": 55,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 7.53,
                "deg": 32,
                "gust": 12.7
            },
            "visibility": 10000,
            "pop": 0.31,
            "rain": {
                "3h": 0.56
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-12 15:00:00"
        },
        {
            "dt": 1678644000,
            "main": {
                "temp": 18.27,
                "feels_like": 17.41,
                "temp_min": 18.27,
                "temp_max": 18.27,
                "pressure": 1023,
                "sea_level": 1023,
                "grnd_level": 1022,
                "humidity": 48,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 6.38,
                "deg": 25,
                "gust": 10.63
            },
            "visibility": 10000,
            "pop": 0.31,
            "rain": {
                "3h": 0.42
            },
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-12 18:00:00"
        },
        {
            "dt": 1678654800,
            "main": {
                "temp": 17.81,
                "feels_like": 16.85,
                "temp_min": 17.81,
                "temp_max": 17.81,
                "pressure": 1023,
                "sea_level": 1023,
                "grnd_level": 1021,
                "humidity": 46,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 5.7,
                "deg": 20,
                "gust": 9.47
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-12 21:00:00"
        },
        {
            "dt": 1678665600,
            "main": {
                "temp": 15.82,
                "feels_like": 15,
                "temp_min": 15.82,
                "temp_max": 15.82,
                "pressure": 1025,
                "sea_level": 1025,
                "grnd_level": 1023,
                "humidity": 59,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 6.65,
                "deg": 20,
                "gust": 10.47
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-13 00:00:00"
        },
        {
            "dt": 1678676400,
            "main": {
                "temp": 15.84,
                "feels_like": 15.07,
                "temp_min": 15.84,
                "temp_max": 15.84,
                "pressure": 1025,
                "sea_level": 1025,
                "grnd_level": 1024,
                "humidity": 61,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 6.17,
                "deg": 24,
                "gust": 8.93
            },
            "visibility": 10000,
            "pop": 0.01,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-13 03:00:00"
        },
        {
            "dt": 1678687200,
            "main": {
                "temp": 16.59,
                "feels_like": 15.85,
                "temp_min": 16.59,
                "temp_max": 16.59,
                "pressure": 1022,
                "sea_level": 1022,
                "grnd_level": 1021,
                "humidity": 59,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 4.47,
                "deg": 17,
                "gust": 5.63
            },
            "visibility": 10000,
            "pop": 0.01,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-13 06:00:00"
        },
        {
            "dt": 1678698000,
            "main": {
                "temp": 17.51,
                "feels_like": 16.8,
                "temp_min": 17.51,
                "temp_max": 17.51,
                "pressure": 1019,
                "sea_level": 1019,
                "grnd_level": 1018,
                "humidity": 57,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 3.42,
                "deg": 27,
                "gust": 3.96
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-13 09:00:00"
        },
        {
            "dt": 1678708800,
            "main": {
                "temp": 16.77,
                "feels_like": 16.07,
                "temp_min": 16.77,
                "temp_max": 16.77,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1018,
                "humidity": 60,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.97,
                "deg": 47,
                "gust": 2.58
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-13 12:00:00"
        },
        {
            "dt": 1678719600,
            "main": {
                "temp": 16.49,
                "feels_like": 15.79,
                "temp_min": 16.49,
                "temp_max": 16.49,
                "pressure": 1020,
                "sea_level": 1020,
                "grnd_level": 1018,
                "humidity": 61,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.53,
                "deg": 81,
                "gust": 2.04
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-13 15:00:00"
        },
        {
            "dt": 1678730400,
            "main": {
                "temp": 15.7,
                "feels_like": 15.02,
                "temp_min": 15.7,
                "temp_max": 15.7,
                "pressure": 1018,
                "sea_level": 1018,
                "grnd_level": 1016,
                "humidity": 65,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 0.96,
                "deg": 89,
                "gust": 1.2
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-13 18:00:00"
        },
        {
            "dt": 1678741200,
            "main": {
                "temp": 15.4,
                "feels_like": 14.72,
                "temp_min": 15.4,
                "temp_max": 15.4,
                "pressure": 1016,
                "sea_level": 1016,
                "grnd_level": 1015,
                "humidity": 66,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 0.97,
                "deg": 80,
                "gust": 1.1
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-13 21:00:00"
        },
        {
            "dt": 1678752000,
            "main": {
                "temp": 15.78,
                "feels_like": 15.14,
                "temp_min": 15.78,
                "temp_max": 15.78,
                "pressure": 1017,
                "sea_level": 1017,
                "grnd_level": 1015,
                "humidity": 66,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 95
            },
            "wind": {
                "speed": 0.97,
                "deg": 135,
                "gust": 1.39
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-14 00:00:00"
        },
        {
            "dt": 1678762800,
            "main": {
                "temp": 21.26,
                "feels_like": 20.75,
                "temp_min": 21.26,
                "temp_max": 21.26,
                "pressure": 1017,
                "sea_level": 1017,
                "grnd_level": 1015,
                "humidity": 50,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 76
            },
            "wind": {
                "speed": 2.39,
                "deg": 162,
                "gust": 3.12
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-14 03:00:00"
        },
        {
            "dt": 1678773600,
            "main": {
                "temp": 26.49,
                "feels_like": 26.49,
                "temp_min": 26.49,
                "temp_max": 26.49,
                "pressure": 1012,
                "sea_level": 1012,
                "grnd_level": 1011,
                "humidity": 41,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "clouds": {
                "all": 48
            },
            "wind": {
                "speed": 3.75,
                "deg": 173,
                "gust": 4.6
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-14 06:00:00"
        },
        {
            "dt": 1678784400,
            "main": {
                "temp": 27.97,
                "feels_like": 27.64,
                "temp_min": 27.97,
                "temp_max": 27.97,
                "pressure": 1009,
                "sea_level": 1009,
                "grnd_level": 1007,
                "humidity": 40,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": {
                "all": 0
            },
            "wind": {
                "speed": 5.25,
                "deg": 151,
                "gust": 5.37
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-14 09:00:00"
        },
        {
            "dt": 1678795200,
            "main": {
                "temp": 21.71,
                "feels_like": 21.61,
                "temp_min": 21.71,
                "temp_max": 21.71,
                "pressure": 1011,
                "sea_level": 1011,
                "grnd_level": 1010,
                "humidity": 64,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "clouds": {
                "all": 0
            },
            "wind": {
                "speed": 6.12,
                "deg": 143,
                "gust": 9.73
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-14 12:00:00"
        },
        {
            "dt": 1678806000,
            "main": {
                "temp": 19.69,
                "feels_like": 19.73,
                "temp_min": 19.69,
                "temp_max": 19.69,
                "pressure": 1013,
                "sea_level": 1013,
                "grnd_level": 1012,
                "humidity": 77,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "clouds": {
                "all": 4
            },
            "wind": {
                "speed": 4.46,
                "deg": 145,
                "gust": 9.63
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-14 15:00:00"
        },
        {
            "dt": 1678816800,
            "main": {
                "temp": 19.32,
                "feels_like": 19.53,
                "temp_min": 19.32,
                "temp_max": 19.32,
                "pressure": 1012,
                "sea_level": 1012,
                "grnd_level": 1011,
                "humidity": 85,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "clouds": {
                "all": 13
            },
            "wind": {
                "speed": 3.11,
                "deg": 129,
                "gust": 7.17
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-14 18:00:00"
        },
        {
            "dt": 1678827600,
            "main": {
                "temp": 20.03,
                "feels_like": 20.31,
                "temp_min": 20.03,
                "temp_max": 20.03,
                "pressure": 1012,
                "sea_level": 1012,
                "grnd_level": 1011,
                "humidity": 85,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 98
            },
            "wind": {
                "speed": 2.12,
                "deg": 141,
                "gust": 4.51
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-14 21:00:00"
        },
        {
            "dt": 1678838400,
            "main": {
                "temp": 20.55,
                "feels_like": 20.85,
                "temp_min": 20.55,
                "temp_max": 20.55,
                "pressure": 1015,
                "sea_level": 1015,
                "grnd_level": 1013,
                "humidity": 84,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 99
            },
            "wind": {
                "speed": 1.45,
                "deg": 141,
                "gust": 2.81
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-15 00:00:00"
        },
        {
            "dt": 1678849200,
            "main": {
                "temp": 21.78,
                "feels_like": 22.08,
                "temp_min": 21.78,
                "temp_max": 21.78,
                "pressure": 1016,
                "sea_level": 1016,
                "grnd_level": 1014,
                "humidity": 79,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 1.43,
                "deg": 142,
                "gust": 2.44
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-15 03:00:00"
        },
        {
            "dt": 1678860000,
            "main": {
                "temp": 23.88,
                "feels_like": 24.2,
                "temp_min": 23.88,
                "temp_max": 23.88,
                "pressure": 1014,
                "sea_level": 1014,
                "grnd_level": 1012,
                "humidity": 72,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 2.76,
                "deg": 124,
                "gust": 3.96
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-15 06:00:00"
        },
        {
            "dt": 1678870800,
            "main": {
                "temp": 23.55,
                "feels_like": 23.97,
                "temp_min": 23.55,
                "temp_max": 23.55,
                "pressure": 1012,
                "sea_level": 1012,
                "grnd_level": 1011,
                "humidity": 77,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": {
                "all": 100
            },
            "wind": {
                "speed": 4.51,
                "deg": 115,
                "gust": 5.77
            },
            "visibility": 10000,
            "pop": 0.2,
            "rain": {
                "3h": 0.15
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2023-03-15 09:00:00"
        },
        {
            "dt": 1678881600,
            "main": {
                "temp": 21.99,
                "feels_like": 22.44,
                "temp_min": 21.99,
                "temp_max": 21.99,
                "pressure": 1014,
                "sea_level": 1014,
                "grnd_level": 1012,
                "humidity": 84,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "clouds": {
                "all": 94
            },
            "wind": {
                "speed": 3.6,
                "deg": 120,
                "gust": 7.68
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-15 12:00:00"
        },
        {
            "dt": 1678892400,
            "main": {
                "temp": 20.55,
                "feels_like": 21.01,
                "temp_min": 20.55,
                "temp_max": 20.55,
                "pressure": 1015,
                "sea_level": 1015,
                "grnd_level": 1013,
                "humidity": 90,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "clouds": {
                "all": 22
            },
            "wind": {
                "speed": 3.27,
                "deg": 127,
                "gust": 8.56
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "n"
            },
            "dt_txt": "2023-03-15 15:00:00"
        }
    ],
    "city": {
        "id": 1581130,
        "name": "",
        "coord": {
            "lat": 21.0245,
            "lon": 105.8412
        },
        "country": "VN",
        "population": 1431270,
        "timezone": 25200,
        "sunrise": 1678403420,
        "sunset": 1678446246
    }
}`
const initialState = {
    city:"",
    showInfoWeather: false,
    cityWeather: JSON.parse(JSON_WEATHER),
    days:  [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday']
}

//hàm chuyên đổi tiếng việt sang không dấu và uppercase
function toLowerCaseNonAccentVietnamese(str) {
    str = str.toLowerCase();
//     We can also use this instead of from line 11 to line 17
//     str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
//     str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
//     str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
//     str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
//     str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
//     str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
//     str = str.replace(/\u0111/g, "d");
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng 
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
    return str;
}
const weatherReducer = (state = initialState, action) =>{
    switch(action.type){
        case USER_TYPE_AND_CLICK:
            state.city = toLowerCaseNonAccentVietnamese(action.payload);
            break;
        case SEND_REQUEST_WEATHER_DATA_PENDING:
            console.log("hello");
            break;
        case SEND_REQUEST_WEATHER_DATA_SUCCESS:
            state.cityWeather = action.payload;
            if(state.cityWeather.city.name !== ""){
                state.showInfoWeather = true;
            }
            console.log(state.cityWeather);
            break;
        case SEND_REQUEST_WEATHER_DATA_FAIL:
            console.log(action.error);
            break;
        default:
            break;
    }
    return{...state};
} 
export default weatherReducer;